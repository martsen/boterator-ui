FROM node

RUN npm install -g gulp

ADD . /opt/webogram
WORKDIR /opt/webogram

RUN npm install

EXPOSE 8000

CMD ["gulp", "watch"]
